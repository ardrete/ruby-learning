class Player

  attr_reader :health
  attr_accessor :name

  def initialize(name, health = 100)
    @name = name.capitalize
    @health = health
    @treasures = Hash.new(0)
  end

  def points
    @treasures.values.reduce(0, :+)
  end

  def found_treasure(treasure)
    @treasures[treasure.name] += treasure.points
    puts "#{@name} found #{treasure.name} for #{treasure.points}"
    puts "#{@name}'s treasures: #{@treasures}"
  end

  def score
    @health + points
  end

  def to_s
    "Hi my name is #{@name} and my health is #{@health} and the score is #{score}"
  end

  def blam
    @health -= 10
    puts "#{@name} got blammed!"
  end

  def w00t
    @health += 15
    puts "#{@name} was w00ted!"
  end

  def name=(ncname)
    @name = ncname.capitalize
  end

  def strong?
    @health > 100
  end

  def <=>(other)
    other.score <=> score
  end

  def to_s
    "I'm #{@name} with health #{@health} , points #{points} and score of #{score}"
  end

end


if __FILE__ == $0
  player3 = Player.new("Monk")
  player4 = Player.new("Juris")

  puts player4
  puts player3

end