require 'rspec'
require_relative 'game'

describe Game do
  before do
    @game = Game.new("Hives")
    @initial_health = 100
    @player = Player.new("Jerry", @initial_health)
    @rounds = 3
    @game.add_player(@player)
  end

  it "a higher number is rolled" do
    # Old syntax
    # Die.any_instance.stub(:roll).and_return(5)

    # Rspec 3.0
    allow_any_instance_of(Die).to receive(:roll).and_return(5)

    @game.play(@rounds)
    health = @initial_health + (15.* @rounds)
    expect(@player.health).to equal(health)
  end

  it "skips players if a medium number is rolled" do
    allow_any_instance_of(Die).to receive(:roll).and_return(3)

    @game.play(@rounds)

    expect(@player.health).to equal(@initial_health)
  end


  it "a lower number is rolled" do
    allow_any_instance_of(Die).to receive(:roll).and_return(1)

    @game.play(@rounds)
    health = @initial_health - (10.* @rounds)
    expect(@player.health).to equal(health)
  end

  it "assigns a treasure for points during a player's turn" do
    game = Game.new("Knuckleheads")
    player = Player.new("moe")

    game.add_player(player)

    game.play(1)

    # player.points.should_not be_zero

    # or use alternate expectation syntax:
    expect(player.points).not_to be_zero
  end

end