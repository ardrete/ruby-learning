require_relative 'player'
require_relative 'die'
require_relative 'game_turn'
require_relative 'treasure_trove'

class Game
  attr_reader :title

  def initialize(name)
    @title = name
    @players = []
  end

  def play(rounds)
    treasures = TreasureTrove::TREASURES

    puts "They are #{treasures.size} treasures"
    treasures.each { |t| puts "A treasure #{t.name} is worth  #{t.points} points" }

    puts "\nJuego de #{@title}"
    puts "They are #{@players.size} players in the game"
    @players.each do |player|
      puts player
    end

    1.upto(rounds) do |round|
      puts "\nRound #{round}"
      @players.each do |player|
        GameTurn.take_turn(player)
        puts player
      end
    end
    # If syntax
    # if number_rolled < 3
    #   player.blam
    # elsif number_rolled < 5
    #   puts "#{player.name} was skipped"
    # else
    #   player.w00t
    # end

  end

  def add_player(player)
    if player.instance_of?(Player)
      @players.push(player)
    end
  end

  def print_name_and_health(player)
    formatted_name = player.name.ljust(20, '.')
    puts "#{formatted_name} #{player.score}"
  end

  def print_stats
    strong, wimpy = @players.partition { |p| p.strong? }

    puts "\n#{@title} Statistics"

    @players.each do |p|
      puts "\n #{p.name}'s points total:'"
      puts "#{p.points} grand total points"
    end

    puts "\n#{strong.size} strong players:"
    strong.each do |player|
      puts "#{player.name} (#{player.health})"
    end

    puts "\n#{wimpy.size} wimpy players:"
    wimpy.each do |player|
      puts "#{player.name} (#{player.health})"
    end


    puts "\nHigh Scores:"

    players_sorted = @players.sort

    players_sorted.each do |p|
      print_name_and_health(p)
    end

  end

end