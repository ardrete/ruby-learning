require_relative 'player'
require_relative 'treasure_trove'

describe Player do
  before do
    $stdout = StringIO.new
    @initial_health = 30
    @player = Player.new('mess', @initial_health)
  end

  it 'should has capitalized name' do
    expect(@player.name).to eq("Mess")

  end

  it 'has a string representation' do
    expect(@player.to_s).to eq("Hi my name is Mess and my health is 30 and the score is 34")
  end

  it 'has a initial health' do
    expect(@player.health).to eq(@initial_health)
  end

  it 'increases health by 15 when w00ted' do
    @player.w00t

    expect(@player.health).to eq(@initial_health + 15)
  end

  it 'decreases health by 10 when blammed' do
    @player.blam
    expect(@player.health).to eq(@initial_health - 10)
  end


  context "with a health greater than 100 " do
    before do
      @player = Player.new("Harry" ,150)
    end

    it "is strong" do
      # expect(@player.strong?).to eq(true)
      expect(@player).to be_strong
    end
  end

  context "with a health less than 100" do
    before do
      @player = Player.new("Larry", 50)
    end

    it "is wimppy" do
      expect(@player).not_to be_strong
    end
  end

  context "in a collection of players" do
    before do
      @player1 = Player.new("moe", 100)
      @player2 = Player.new("larry", 200)
      @player3 = Player.new("curly", 300)

      @players = [@player1, @player2, @player3]
    end

    it "is sorted by decreasing score" do
      expect(@players.sort).to  eq([@player3, @player2, @player1])
    end
  end


  it "computes points as the sum of all treasure points" do
    expect(@player.points).to  eq(0)

    @player.found_treasure(Treasure.new(:hammer, 50))

    expect(@player.points).to  eq(50)

    @player.found_treasure(Treasure.new(:crowbar, 400))

    expect(@player.points).to eq(450)

    @player.found_treasure(Treasure.new(:hammer, 50))

    expect(@player.points).to eq(500)
  end

  it "computes a score as the sum of its health and points" do
    @player.found_treasure(Treasure.new(:hammer, 50))
    @player.found_treasure(Treasure.new(:hammer, 50))

    expect(@player.score).to eq(130)

    expect(@player.to_s).to eq("I'm Mess with health 30 , points 100 and score of 130")
  end

end


